package com.example.androidstudio.prudentefinalexamy;

/**
 * Created by androidstudio on 07/10/2017.
 */
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class splashtictactoe extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        ImageView firstimage = (ImageView)findViewById(R.id.img1);
        firstimage.animate().rotationBy(3600f).setDuration(2000);

        Thread timeThread = new Thread(){
            public void run(){
                try{
                    sleep(2000);
                }
                catch(InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    Intent intent = new Intent(splashtictactoe.this, TictacMain.class);
                    startActivity(intent);
                    finish();
                }
            }
        };
        timeThread.start();
    }

}
