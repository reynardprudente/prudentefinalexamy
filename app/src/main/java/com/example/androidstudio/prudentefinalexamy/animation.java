package com.example.androidstudio.prudentefinalexamy;

/**
 * Created by androidstudio on 07/10/2017.
 */
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class animation extends AppCompatActivity {

    public void fade(View view){
        ImageView i = (ImageView) findViewById(R.id.img1);
        ImageView c = (ImageView)findViewById(R.id.img2);
        if (i.getAlpha()==1){
            i.animate().alpha(0f).rotationBy(3600).setDuration(2000);
            c.animate().alpha(1f).rotationBy(3600).setDuration(2000);
        }
        else if (i.getAlpha()==0){
            c.animate().alpha(0f).rotationBy(3600).setDuration(2000);
            i.animate().alpha(1f).rotationBy(3600).setDuration(2000);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animation);
    }
}
