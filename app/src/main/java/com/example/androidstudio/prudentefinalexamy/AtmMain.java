package com.example.androidstudio.prudentefinalexamy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class AtmMain extends AppCompatActivity {
    String balacc,fname,pin,pinlogg,acc;
    EditText pinlog;
    Button login;
    TextView register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.atmlogin);

        Bundle bundle = getIntent().getExtras();
        fname = bundle.getString("fname");
        balacc = bundle.getString("balnum");
        pin = bundle.getString("pin");
        acc = bundle.getString("acc");
        login = (Button) findViewById(R.id.button8);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pinlog = (EditText) findViewById(R.id.editText5);
                pinlogg = pinlog.getText().toString();
                if(pinlogg.equals(pin)){
                    Intent intent = new Intent(AtmMain.this,AtmMainBal.class);
                    intent.putExtra("balacc",balacc);
                    intent.putExtra("fname",fname);
                    startActivity(intent);
                }
            }
        });

        register = (TextView) findViewById(R.id.textView3);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AtmMain.this,atmregister.class);
                startActivity(intent);
            }
        });


    }
}
