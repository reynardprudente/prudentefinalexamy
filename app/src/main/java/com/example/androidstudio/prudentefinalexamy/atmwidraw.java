package com.example.androidstudio.prudentefinalexamy;

/**
 * Created by androidstudio on 07/10/2017.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class atmwidraw extends AppCompatActivity {
    String curbal,withdraw,fname;
    TextView curbaltxt,withdrawtxt,fnametxt;
    Button done;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.atmwidraw);

        Bundle bundle = getIntent().getExtras();

        fname = bundle.getString("fname");
        curbal = bundle.getString("curbaltotal");
        withdraw = bundle.getString("withdraw");

        curbaltxt = (TextView) findViewById(R.id.textView24);
        withdrawtxt = (TextView) findViewById(R.id.textView28);
        fnametxt = (TextView) findViewById(R.id.textView22);

        curbaltxt.setText(curbal);
        withdrawtxt.setText(withdraw);
        fnametxt.setText(fname);
        done = (Button) findViewById(R.id.button11);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(atmwidraw.this,AtmMainBal.class);
                startActivity(intent);
                finish();
            }
        });
    }
}

