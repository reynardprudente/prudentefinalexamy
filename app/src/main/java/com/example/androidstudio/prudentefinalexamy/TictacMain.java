package com.example.androidstudio.prudentefinalexamy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;



public class TictacMain extends AppCompatActivity {
    EditText player1,player2,randnum;
    Button btn_rand;
    CheckBox chkbox;
    String play1,play2,round,round2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tictac_main);
        chkbox = (CheckBox) findViewById(R.id.checkBox2);
        randnum = (EditText) findViewById(R.id.editText2);



        chkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(chkbox.isChecked()){

                    Toast.makeText(getApplicationContext(), "Dev Mode", Toast.LENGTH_LONG).show();
                    randnum.setEnabled(true);

                }
                else{
                    Toast.makeText(getApplicationContext(), "Test Mode", Toast.LENGTH_LONG).show();
                    randnum.setEnabled(false);
                }
            }
        });

        btn_rand = (Button) findViewById(R.id.button1);
        btn_rand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                player1 = (EditText) findViewById(R.id.edittext1);
                player2 = (EditText) findViewById(R.id.edittext2);
                play1 = player1.getText().toString();
                play2 = player2.getText().toString();
                if(play1 == null || play2 == null){
                    Toast.makeText(getApplicationContext(), "Enter player 1 or player 2 name", Toast.LENGTH_LONG).show();

                }else {
                    if (chkbox.isChecked()) {
                        round = randnum.getText().toString();
                        Intent intent = new Intent(TictacMain.this, TictacMain2.class);
                        intent.putExtra("play1", play1);
                        intent.putExtra("play2", play2);
                        intent.putExtra("round", round);
                        startActivity(intent);
                        finish();
                    } else {
                        ArrayList<Integer> lista = new ArrayList<Integer>();
                        lista.add(3);
                        lista.add(5);
                        lista.add(7);

                        Random r = new Random();
                        round2 = String.valueOf(lista.get(r.nextInt(lista.size())));
                        Intent intent = new Intent(TictacMain.this, TictacMain2.class);
                        intent.putExtra("play1", play1);
                        intent.putExtra("play2", play2);
                        intent.putExtra("round", round2);
                        startActivity(intent);
                        finish();
                    }

                }

            }


        });
    }


}
