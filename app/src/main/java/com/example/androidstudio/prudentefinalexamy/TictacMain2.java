package com.example.androidstudio.prudentefinalexamy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



public class TictacMain2 extends AppCompatActivity {
    ImageView imgv1,imgv2,imgv3,imgv4,imgv5,imgv6,imgv7,imgv8,imgv9,imgvBoard;
    int a1,a2,a3,a4,a5,a6,a7,a8,a9, b1,b2,b3,b4,b5,b6,b7,b8,b9;
    int check,scoreplay1,scoreplay2,win1,win2,roundcon,play1total,play2total,roundcon2;
    String play1,play2,roundwin,roundwin2;
    Button newGame,restart,exit;
    boolean move=true;
    TextView textView1,textView2,score1,score2,best,roundcont;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tictac_main2);
        exit = (Button) findViewById(R.id.button3);
        textView1 = (TextView) findViewById(R.id.textView1);
        textView2 = (TextView) findViewById(R.id.textView2);
        score1 = (TextView) findViewById(R.id.textView3);
        score2= (TextView) findViewById(R.id.textView4);
        best = (TextView) findViewById(R.id.textView6);
        roundcont = (TextView) findViewById(R.id.textView7);
        Bundle extras = getIntent().getExtras();
        play1 = extras.getString("play1");
        play2 = extras.getString("play2");
        roundwin = extras.getString("round");
        win1= Integer.parseInt(roundwin);
        textView1.setText(play1+":");
        textView2.setText(play2+":");
        best.setText("Best of "+ win1);

        newGame = (Button)findViewById(R.id.button);
        restart = (Button) findViewById(R.id.button2);
        imgvBoard = (ImageView)findViewById(R.id.ivBoard);
        imgv1 = (ImageView)findViewById(R.id.iv1_1);
        imgv2 = (ImageView)findViewById(R.id.iv1_2);
        imgv3 = (ImageView)findViewById(R.id.iv1_3);
        imgv4 = (ImageView)findViewById(R.id.iv2_1);
        imgv5 = (ImageView)findViewById(R.id.iv2_2);
        imgv6 = (ImageView)findViewById(R.id.iv2_3);
        imgv7 = (ImageView)findViewById(R.id.iv3_1);
        imgv8 = (ImageView)findViewById(R.id.iv3_2);
        imgv9 = (ImageView)findViewById(R.id.iv3_3);
        newGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgv1.setAlpha(0f);
                imgv2.setAlpha(0f);
                imgv3.setAlpha(0f);
                imgv4.setAlpha(0f);
                imgv5.setAlpha(0f);
                imgv6.setAlpha(0f);
                imgv7.setAlpha(0f);
                imgv8.setAlpha(0f);
                imgv9.setAlpha(0f);
                imgv1.setEnabled(true);
                imgv2.setEnabled(true);
                imgv3.setEnabled(true);
                imgv4.setEnabled(true);
                imgv5.setEnabled(true);
                imgv6.setEnabled(true);
                imgv7.setEnabled(true);
                imgv8.setEnabled(true);
                imgv9.setEnabled(true);
                a1=0;a2=0;a3=0;a4=0;a5=0;a6=0;a7=0;a8=0;a9=0;
                b1=0;b2=0;b3=0;b4=0;b5=0;b6=0;b7=0;b8=0;b9=0;
                move=true;
                check=0;
                scoreplay1=0;
                scoreplay2=0;
                score1.setText("");
                score2.setText("");
                roundcon = 1;
                roundcont.setText("Round: 1");
                restart.setVisibility(View.INVISIBLE);
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgv1.setAlpha(0f);
                imgv2.setAlpha(0f);
                imgv3.setAlpha(0f);
                imgv4.setAlpha(0f);
                imgv5.setAlpha(0f);
                imgv6.setAlpha(0f);
                imgv7.setAlpha(0f);
                imgv8.setAlpha(0f);
                imgv9.setAlpha(0f);
                imgv1.setEnabled(true);
                imgv2.setEnabled(true);
                imgv3.setEnabled(true);
                imgv4.setEnabled(true);
                imgv5.setEnabled(true);
                imgv6.setEnabled(true);
                imgv7.setEnabled(true);
                imgv8.setEnabled(true);
                imgv9.setEnabled(true);
                a1=0;a2=0;a3=0;a4=0;a5=0;a6=0;a7=0;a8=0;a9=0;
                b1=0;b2=0;b3=0;b4=0;b5=0;b6=0;b7=0;b8=0;b9=0;
                move=true;
                check = 0;
                roundcon = roundcon+1;
                restart.setVisibility(View.INVISIBLE);
                roundcont.setText("Round: "+ String.valueOf(roundcon));
            }
        });
        imgv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (move) {
                    imgv1.setImageResource(R.drawable.s);
                    imgv1.setAlpha(1f);
                    a1=1;
                    move = false;
                }
                else {
                    imgv1.setImageResource(R.drawable.b);
                    imgv1.setAlpha(1f);
                    b1=1;
                    move = true;
                }
                check=check+1;
                imgv1.setEnabled(false);
                if(check==9) {
                    drawMessage();
                }
                else{
                    checkWinner();
                }
            }
        });
        imgv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (move) {
                    imgv2.setImageResource(R.drawable.s);
                    imgv2.setAlpha(1f);
                    a2=1;
                    move = false;
                }
                else {
                    imgv2.setImageResource(R.drawable.b);
                    imgv2.setAlpha(1f);
                    b2=1;
                    move = true;
                }
                check=check+1;
                imgv2.setEnabled(false);
                if(check==9) {
                    drawMessage();
                }
                else{
                    checkWinner();
                }
            }
        });
        imgv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (move) {
                    imgv3.setImageResource(R.drawable.s);
                    imgv3.setAlpha(1f);
                    a3=1;
                    move = false;
                }
                else {
                    imgv3.setImageResource(R.drawable.b);
                    imgv3.setAlpha(1f);
                    b3=1;
                    move = true;
                }
                check=check+1;
                imgv3.setEnabled(false);
                if(check==9) {
                    drawMessage();
                }
                else{
                    checkWinner();
                }
            }
        });
        imgv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (move) {
                    imgv4.setImageResource(R.drawable.s);
                    imgv4.setAlpha(1f);
                    a4=1;
                    move = false;
                }
                else {
                    imgv4.setImageResource(R.drawable.b);
                    imgv4.setAlpha(1f);
                    b4=1;
                    move = true;
                }
                check=check+1;
                imgv4.setEnabled(false);
                if(check==10) {
                    drawMessage();
                }
                else{
                    checkWinner();
                }
            }
        });
        imgv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (move) {
                    imgv5.setImageResource(R.drawable.s);
                    imgv5.setAlpha(1f);
                    a5=1;
                    move = false;
                }
                else {
                    imgv5.setImageResource(R.drawable.b);
                    imgv5.setAlpha(1f);
                    b5=1;
                    move = true;
                }
                check=check+1;
                imgv5.setEnabled(false);
                if(check==9) {
                    drawMessage();
                }
                else{
                    checkWinner();
                }
            }
        });
        imgv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (move) {
                    imgv6.setImageResource(R.drawable.s);
                    imgv6.setAlpha(1f);
                    a6=1;
                    move = false;
                }
                else {
                    imgv6.setImageResource(R.drawable.b);
                    imgv6.setAlpha(1f);
                    b6=1;
                    move = true;
                }
                check=check+1;
                imgv6.setEnabled(false);
                if(check==9) {
                    drawMessage();
                }
                else{
                    checkWinner();
                }
            }
        });
        imgv7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (move) {
                    imgv7.setImageResource(R.drawable.s);
                    imgv7.setAlpha(1f);
                    a7=1;
                    move = false;
                }
                else {
                    imgv7.setImageResource(R.drawable.b);
                    imgv7.setAlpha(1f);
                    b7=1;
                    move = true;
                }
                check=check+1;
                imgv7.setEnabled(false);
                if(check==9) {
                    drawMessage();
                }
                else{
                    checkWinner();
                }            }
        });
        imgv8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (move) {
                    imgv8.setImageResource(R.drawable.s);
                    imgv8.setAlpha(1f);
                    a8=1;
                    move = false;
                }
                else {
                    imgv8.setImageResource(R.drawable.b);
                    imgv8.setAlpha(1f);
                    b8=1;
                    move = true;
                }
                check=check+1;
                imgv8.setEnabled(false);
                if(check==9) {
                    drawMessage();
                }
                else{
                    checkWinner();
                }            }
        });
        imgv9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (move) {
                    imgv9.setImageResource(R.drawable.s);
                    imgv9.setAlpha(1f);
                    a9=1;
                    move = false;
                }
                else {
                    imgv9.setImageResource(R.drawable.b);
                    imgv9.setAlpha(1f);
                    b9=1;
                    move = true;
                }
                check=check+1;
                imgv9.setEnabled(false);
                if(check==9) {
                    drawMessage();
                }
                else{
                    checkWinner();
                }            }
        });
    }
    public void checkWinner(){
        if (a1+a2+a3==3|a4+a5+a6==3|a7+a8+a9==3|a1+a5+a9==3|a7+a5+a3==3|a1+a4+a7==3|a2+a5+a8==3|a3+a6+a9==3){
            Toast.makeText(this, play1+" wins.", Toast.LENGTH_SHORT).show();
            imgv1.setEnabled(false);
            imgv2.setEnabled(false);
            imgv3.setEnabled(false);
            imgv4.setEnabled(false);
            imgv5.setEnabled(false);
            imgv6.setEnabled(false);
            imgv7.setEnabled(false);
            imgv8.setEnabled(false);
            imgv9.setEnabled(false);
            scoreplay1 = scoreplay1+1;
            score1.setText(String.valueOf(scoreplay1));
            restart.setVisibility(View.VISIBLE);

            if(win1 == 3) {
                if (scoreplay1 == 2) {
                    Toast.makeText(this, play1 + " wins the game.", Toast.LENGTH_LONG).show();
                    restart.setVisibility(View.INVISIBLE);

                }
            }
            else if (win1 == 5){
                if(scoreplay1 == 3){
                    Toast.makeText(this, play1 + " wins the game.", Toast.LENGTH_LONG).show();
                    restart.setVisibility(View.INVISIBLE);

                }
            }
            else if( win1 == 7) {
                if (scoreplay1 == 4) {
                    Toast.makeText(this, play1 + " wins the game.", Toast.LENGTH_LONG).show();
                    restart.setVisibility(View.INVISIBLE);

                }
            }
        }

        else if (b1+b2+b3==3|b4+b5+b6==3|b7+b8+b9==3|b1+b5+b9==3|b7+b5+b3==3|b1+b4+b7==3|b2+b5+b8==3|b3+b6+b9==3) {
            Toast.makeText(this, play2 + " wins.", Toast.LENGTH_SHORT).show();
            imgv1.setEnabled(false);
            imgv2.setEnabled(false);
            imgv3.setEnabled(false);
            imgv4.setEnabled(false);
            imgv5.setEnabled(false);
            imgv6.setEnabled(false);
            imgv7.setEnabled(false);
            imgv8.setEnabled(false);
            imgv9.setEnabled(false);

            scoreplay2 = scoreplay2 + 1;
            score2.setText(String.valueOf(scoreplay2));
            restart.setVisibility(View.VISIBLE);

            if ( win1 == 3) {
                if (scoreplay2 == 2) {
                    Toast.makeText(this, play2 + " wins the game.", Toast.LENGTH_LONG).show();
                    restart.setVisibility(View.INVISIBLE);

                }
            } else if (win1 == 5) {
                if (scoreplay2 == 3) {
                    Toast.makeText(this, play2 + " wins the game.", Toast.LENGTH_LONG).show();
                    restart.setVisibility(View.INVISIBLE);

                }
            } else if (win1 == 7) {
                if (scoreplay2 == 4) {
                    Toast.makeText(this, play2 + " wins the game.", Toast.LENGTH_LONG).show();
                    restart.setVisibility(View.INVISIBLE);

                }
            }
        }
    }

    public void drawMessage(){
        imgv1.setAlpha(0f);
        imgv2.setAlpha(0f);
        imgv3.setAlpha(0f);
        imgv4.setAlpha(0f);
        imgv5.setAlpha(0f);
        imgv6.setAlpha(0f);
        imgv7.setAlpha(0f);
        imgv8.setAlpha(0f);
        imgv9.setAlpha(0f);
        imgv1.setEnabled(true);
        imgv2.setEnabled(true);
        imgv3.setEnabled(true);
        imgv4.setEnabled(true);
        imgv5.setEnabled(true);
        imgv6.setEnabled(true);
        imgv7.setEnabled(true);
        imgv8.setEnabled(true);
        imgv9.setEnabled(true);
        a1=0;a2=0;a3=0;a4=0;a5=0;a6=0;a7=0;a8=0;a9=0;
        b1=0;b2=0;b3=0;b4=0;b5=0;b6=0;b7=0;b8=0;b9=0;
        move=true;
        check = 0;
        Toast.makeText(this, "It's a draw.", Toast.LENGTH_LONG).show();
    }

}
