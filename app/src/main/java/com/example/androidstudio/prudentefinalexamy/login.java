package com.example.androidstudio.prudentefinalexamy;

/**
 * Created by androidstudio on 07/10/2017.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class login extends AppCompatActivity {
    String userlog,passlog,fname;
    Button login;
    EditText user,pass;
    TextView reg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        Bundle bundle = getIntent().getExtras();
        userlog = bundle.getString("usertxt");
        passlog = bundle.getString("passtxt");
        fname = bundle.getString("fname");

        login = (Button) findViewById(R.id.button);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = (EditText) findViewById(R.id.editText);
                pass = (EditText) findViewById(R.id.editText2);
                String userlog1 = user.getText().toString();
                String passlog1 = pass.getText().toString();
                if(userlog1.equals(userlog) && passlog1.equals(passlog)){
                    Intent intent = new Intent(login.this,MenuMain.class);
                    intent.putExtra("fname",fname);
                    startActivity(intent);
                    finish();
                }
                else{
                    Toast.makeText(getApplicationContext(),"WRONG USERNAME OR PASSWORD", Toast.LENGTH_SHORT).show();
                    user.setText("");
                    pass.setText("");
                }
            }
        });

        reg = (TextView) findViewById(R.id.textView2);
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(login.this,RegisterMain.class);
                startActivity(intent);
            }
        });

    }
}

