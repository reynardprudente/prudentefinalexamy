package com.example.androidstudio.prudentefinalexamy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class AtmMainBal extends AppCompatActivity {
    Button checkbal,withdraw,confirm,receipt;
    String curballog,fname,withnumcon,curtotal;
    int curbalmain,withnumtotal,curbaltotal;
    EditText withnum;
    TextView curbal,curbaltxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.atm_mainbal);
        Bundle bundle = getIntent().getExtras();
        curballog = bundle.getString("balacc");
        fname = bundle.getString("fname");

        checkbal = (Button) findViewById(R.id.button7);
        checkbal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curbaltxt = (TextView) findViewById(R.id.textView4);
                curbal = (TextView) findViewById(R.id.textView5);
                curbaltxt.setVisibility(View.VISIBLE);
                curbal.setVisibility(View.VISIBLE);
                curbal.setText(curballog);
            }
        });

        withdraw = (Button) findViewById(R.id.button9);
        confirm = (Button) findViewById(R.id.button10);
        receipt = (Button) findViewById(R.id.button12);
        withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                withnum = (EditText) findViewById(R.id.editText6);
                withnumcon = withnum.getText().toString();
                withnumtotal = Integer.valueOf(withnum.getText().toString());
                withnum.setVisibility(View.VISIBLE);
                confirm.setVisibility(View.VISIBLE);
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withnumtotal < curbaltotal) {
                    curbalmain = Integer.valueOf(curbal.getText().toString());
                    curbaltotal = curbalmain - withnumtotal;
                    curbal.setText(curbaltotal);
                }
                else{
                    Toast.makeText(getApplicationContext(),"INVALID AMOUNT", Toast.LENGTH_SHORT).show();
                    withnum.setText("");
                }
            }
        });
        receipt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AtmMainBal.this,atmregister.class);
                intent.putExtra("withdraw",withnumcon);
                intent.putExtra("curbaltotal",curbaltotal);
                intent.putExtra("fname",fname);
                startActivity(intent);
            }
        });
    }
}
