package com.example.androidstudio.prudentefinalexamy;
/**
 * Created by androidstudio on 07/10/2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class RegisterMain extends AppCompatActivity {

    Button reg;
    EditText fname,lname,uname,pword;
    String fnamelog,unamelog,pwordlog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        reg = (Button) findViewById(R.id.button2);
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fname = (EditText) findViewById(R.id.editText3);
                lname = (EditText) findViewById(R.id.editText4);
                uname = (EditText) findViewById(R.id.editText5);
                pword = (EditText) findViewById(R.id.editText6);

                fnamelog = fname.getText().toString();
                unamelog = lname.getText().toString();
                pwordlog = pword.getText().toString();


                Intent intent = new Intent(RegisterMain.this, login.class);
                intent.putExtra("usertxt",unamelog);
                intent.putExtra("passtxt",pwordlog);
                intent.putExtra("fname",fnamelog);

                startActivity(intent);


            }
        });

    }
}
