package com.example.androidstudio.prudentefinalexamy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class MenuMain extends AppCompatActivity {

    Button animation,atm,triangu,tictactoe;
    String fname;
    TextView fnamepass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);

        Bundle bundle = getIntent().getExtras();
        fname = bundle.getString("fname");

        fnamepass = (TextView) findViewById(R.id.textView);
        fnamepass.setText(fname);

        animation = (Button) findViewById(R.id.button3);
        atm = (Button) findViewById(R.id.button4);
        triangu = (Button) findViewById(R.id.button6);
        tictactoe = (Button) findViewById(R.id.button4);

        animation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuMain.this,animation.class);
                startActivity(intent);

            }
        });

        atm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuMain.this,atmsplash.class);
                intent.putExtra("fname",fname);
                startActivity(intent);


            }
        });

        triangu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuMain.this,triangular.class);
                startActivity(intent);

            }
        });
        tictactoe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuMain.this,splashtictactoe.class);
                intent.putExtra("fname",fname);
                startActivity(intent);


            }
        });
    }
}
